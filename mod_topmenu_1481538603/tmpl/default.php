<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_custom
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript('/modules/mod_custom/js/mod_custom.js') ; 
$document->addStyleSheet('/modules/mod_custom/css/mod_custom.css');  
