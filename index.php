<script src="jquery.js" type="text/javascript"></script>    
 
<form action="zip.php" method="post"  >
<input type="submit" value="creat zip" />
<br />
Name : <br />
From <input type="text" name="oldname"   id="termSearch" value="custom" /> To 
<input type="text" name="name" id="termReplace"  value="myname"   /> |
 <input type="submit" onclick="replacer(); return false;" value="replace with custum" /> <label for="caseSensitive">Case Sensitive</label>
    <input type="checkbox" name="caseSensitive" id="caseSensitive" /><br/>
<hr /><br />
XML : 
<br />
<textarea id="txt1"  id="txt1" class="txtArea"  type="textarea" name="xml" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/mod_custom.xml')) ?> 
</textarea>
<br /><hr /><br />
php : 
<br />
<textarea id="txt2"  class="txtArea"  type="textarea" name="mod" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/mod_custom.php')) ?> 
</textarea>
<br /><br /><hr /><br />
helper : 
<br />
<textarea id="txt3"  class="txtArea"  type="textarea" name="helper" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/helper.php')) ?> 
</textarea>
<br />
<br /><br /><hr /><br />
tmpl : 
<br />
<textarea id="txt4"  class="txtArea"  type="textarea" name="tmpl" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/tmpl/default.php')) ?> 
</textarea>
<br />
<br /><br /><hr /><br />
css : 
<br />
<textarea id="txt5"  class="txtArea"  type="textarea" name="css" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/css/style.css')) ?> 
</textarea>
<br />
js : 
<br />
<textarea id="txt6"  class="txtArea"  type="textarea" name="js" style="width:100%;padding:20px;height:500px;" />
<?php echo(file_get_contents('mod_custum/js/function.js')) ?> 
</textarea>
<br />

</form>



<script type="text/javascript">
	function replacer(){
		for (i = 1; i < 7; i++) {
		var id = '#txt'+i;
		SAR.replaceAll(id);
		//alert(id)
		}
		
	}
	
    var SAR = {};

    SAR.find = function(sobject){
        // collect variables
        var txt = $(sobject).val();
        var strSearchTerm = $("#termSearch").val();
        var isCaseSensitive = ($("#caseSensitive").attr('checked') == 'checked') ? true : false;

        // make text lowercase if search is supposed to be case insensitive
        if(isCaseSensitive == false){
            txt = txt.toLowerCase();
            strSearchTerm = strSearchTerm.toLowerCase();
        }

        // find next index of searchterm, starting from current cursor position
        var cursorPos = ($(sobject).getCursorPosEnd());
        var termPos = txt.indexOf(strSearchTerm, cursorPos);

        // if found, select it
        if(termPos != -1){
            $(sobject).selectRange(termPos, termPos+strSearchTerm.length);
        }else{
            // not found from cursor pos, so start from beginning
            termPos = txt.indexOf(strSearchTerm);
            if(termPos != -1){
                $(sobject).selectRange(termPos, termPos+strSearchTerm.length);
            }else{
                alert("not found");
            }
        }
    };

    SAR.findAndReplace = function(sobject){
        // collect variables
        var origTxt = $(sobject).val(); // needed for text replacement
        var txt = $(sobject).val(); // duplicate needed for case insensitive search
        var strSearchTerm = $("#termSearch").val();
        var strReplaceWith = $("#termReplace").val();
        var isCaseSensitive = ($("#caseSensitive").attr('checked') == 'checked') ? true : false;
        var termPos;

        // make text lowercase if search is supposed to be case insensitive
        if(isCaseSensitive == false){
            txt = txt.toLowerCase();
            strSearchTerm = strSearchTerm.toLowerCase();
        }

        // find next index of searchterm, starting from current cursor position
        var cursorPos = ($(sobject).getCursorPosEnd());
        var termPos = txt.indexOf(strSearchTerm, cursorPos);
        var newText = '';

        // if found, replace it, then select it
        if(termPos != -1){
            newText = origTxt.substring(0, termPos) + strReplaceWith + origTxt.substring(termPos+strSearchTerm.length, origTxt.length)
            $(sobject).val(newText);
            $(sobject).selectRange(termPos, termPos+strReplaceWith.length);
        }else{
            // not found from cursor pos, so start from beginning
            termPos = txt.indexOf(strSearchTerm);
            if(termPos != -1){
                newText = origTxt.substring(0, termPos) + strReplaceWith + origTxt.substring(termPos+strSearchTerm.length, origTxt.length)
                $(sobject).val(newText);
                $(sobject).selectRange(termPos, termPos+strReplaceWith.length);
            }else{
                alert("not found");
            }
        }
    };

    SAR.replaceAll = function(sobject){
        // collect variables
        var origTxt = $(sobject).val(); // needed for text replacement
        var txt = $(sobject).val(); // duplicate needed for case insensitive search
        var strSearchTerm = $("#termSearch").val();
        var strReplaceWith = $("#termReplace").val();
        var isCaseSensitive = ($("#caseSensitive").attr('checked') == 'checked') ? true : false;

        // make text lowercase if search is supposed to be case insensitive
        if(isCaseSensitive == false){
            txt = txt.toLowerCase();
            strSearchTerm = strSearchTerm.toLowerCase();
        }

        // find all occurances of search string
        var matches = [];
        var pos = txt.indexOf(strSearchTerm);
        while(pos > -1) {
            matches.push(pos);
            pos = txt.indexOf(strSearchTerm, pos+1);
        }

        for (var match in matches){
            SAR.findAndReplace(sobject);
        }
    };


    /* TWO UTILITY FUNCTIONS YOU WILL NEED */
    $.fn.selectRange = function(start, end) {
        return this.each(function() {
            if(this.setSelectionRange) {
                this.focus();
                this.setSelectionRange(start, end);
            } else if(this.createTextRange) {
                var range = this.createTextRange();
                range.collapse(true);
                range.moveEnd('character', end);
                range.moveStart('character', start);
                range.select();
            }
        });
    };

    $.fn.getCursorPosEnd = function() {
        var pos = 0;
        var input = this.get(0);
        // IE Support
        if (document.selection) {
            input.focus();
            var sel = document.selection.createRange();
            pos = sel.text.length;
        }
        // Firefox support
        else if (input.selectionStart || input.selectionStart == '0')
            pos = input.selectionEnd;
        return pos;
    };  
</script>
<hr />
<input type="submit" value="creat zip" />