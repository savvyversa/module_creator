<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_header
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript('/modules/mod_header/js/mod_header.js') ; 
$document->addStyleSheet('/modules/mod_header/css/mod_header.css');  
