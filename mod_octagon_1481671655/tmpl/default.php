<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_octagon
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript(JURI::base( true ).'/modules/mod_octagon/js/mod_octagon.js') ; 
$document->addStyleSheet(JURI::base( true ).'/modules/mod_octagon/css/mod_octagon.css');  
