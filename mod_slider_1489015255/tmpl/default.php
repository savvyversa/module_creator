<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_slider
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript(JURI::base( true ).'/modules/mod_slider/js/mod_slider.js') ; 
$document->addStyleSheet(JURI::base( true ).'/modules/mod_slider/css/mod_slider.css');  
