<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_tab_search
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript('/modules/mod_tab_search/js/mod_tab_search.js') ; 
$document->addStyleSheet('/modules/mod_tab_search/css/mod_tab_search.css');  
