<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_teb_items
 *
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Include the menu functions only once
require_once __DIR__ . '/helper.php';

$list       = Modteb_itemsHelper::getList($params);  
 
	require JModuleHelper::getLayoutPath('mod_teb_items');
  
 
