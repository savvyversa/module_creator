<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_teb_items
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript(JURI::base( true ).'/modules/mod_teb_items/js/mod_teb_items.js') ; 
$document->addStyleSheet(JURI::base( true ).'/modules/mod_teb_items/css/mod_teb_items.css');  
