<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_topmenu
 *	savvydesign
 * @copyright   Copyright (C) 2005 - 2016 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

?>



<?php  
$document = JFactory::getDocument();
// Add Javascript 
$document->addScript('/modules/mod_topmenu/js/mod_topmenu.js') ; 
$document->addStyleSheet('/modules/mod_topmenu/css/mod_topmenu.css');  
